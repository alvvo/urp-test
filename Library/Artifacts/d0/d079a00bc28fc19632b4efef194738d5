    "d          2019.4.17f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                      \       ŕyŻ     `                                                                                                                                                                               ŕyŻ                                                                                    PointLightManager   w  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VolumetricFogAndMist2 {

    [ExecuteInEditMode]
    public class PointLightManager : MonoBehaviour, IVolumetricFogManager {

        public string managerName {
            get {
                return "Point Light Manager";
            }
        }

        public const int MAX_POINT_LIGHTS = 16;

        [Header("Point Light Search Settings")]
        [Tooltip("Point lights are sorted by distance to tracking center object")]
        public Transform trackingCenter;
        public float newLightsCheckInterval = 3f;

        [Header("Common Settings")]
        [Tooltip("Global inscattering multiplier for point lights")]
        public float inscattering = 1f;
        [Tooltip("Global intensity multiplier for point lights")]
        public float intensity = 1f;
        [Tooltip("Reduces light intensity near point lights")]
        public float insideAtten;

        Light[] pointLights;
        Vector4[] pointLightColorBuffer;
        Vector4[] pointLightPositionBuffer;
        float checkNewLightsLastTime;

        private void OnEnable() {
            if (trackingCenter == null) {
                Camera cam = null;
                Tools.CheckCamera(ref cam);
                if (cam != null) {
                    trackingCenter = cam.transform;
                }
            }
            if (pointLightColorBuffer == null || pointLightColorBuffer.Length != MAX_POINT_LIGHTS) {
                pointLightColorBuffer = new Vector4[MAX_POINT_LIGHTS];
            }
            if (pointLightPositionBuffer == null || pointLightPositionBuffer.Length != MAX_POINT_LIGHTS) {
                pointLightPositionBuffer = new Vector4[MAX_POINT_LIGHTS];
            }
        }

        private void LateUpdate() {
            TrackPointLights();
            SubmitPointLightData();
        }

        void SubmitPointLightData() {

            int k = 0;
            for (int i = 0; k < MAX_POINT_LIGHTS && i < pointLights.Length; i++) {
                Light light = pointLights[i];
                if (light == null || !light.isActiveAndEnabled || light.type != LightType.Point) continue;
                Vector3 pos = light.transform.position;
                float range = light.range * inscattering / 25f; // note: 25 comes from Unity point light attenuation equation
                float multiplier = light.intensity * intensity;

                if (range > 0 && multiplier > 0) {
                    pointLightPositionBuffer[k].x = pos.x;
                    pointLightPositionBuffer[k].y = pos.y;
                    pointLightPositionBuffer[k].z = pos.z;
                    pointLightPositionBuffer[k].w = 0;
                    Color color = light.color;
                    pointLightColorBuffer[k].x = color.r * multiplier;
                    pointLightColorBuffer[k].y = color.g * multiplier;
                    pointLightColorBuffer[k].z = color.b * multiplier;
                    pointLightColorBuffer[k].w = range;
                    k++;
                }
            }

            Shader.SetGlobalVectorArray(VolumetricFog.ShaderParams.PointLightColors, pointLightColorBuffer);
            Shader.SetGlobalVectorArray(VolumetricFog.ShaderParams.PointLightPositions, pointLightPositionBuffer);
            Shader.SetGlobalFloat(VolumetricFog.ShaderParams.PointLightInsideAtten, insideAtten);
            Shader.SetGlobalInt(VolumetricFog.ShaderParams.PointLightCount, k);
        }

        /// <summary>
        /// Look for nearest point lights
        /// </summary>
        public void TrackPointLights(bool forceImmediateUpdate = false) {

            // Look for new lights?
            if (forceImmediateUpdate || pointLights == null || !Application.isPlaying || (newLightsCheckInterval > 0 && Time.time - checkNewLightsLastTime > newLightsCheckInterval)) {
                checkNewLightsLastTime = Time.time;
                pointLights = FindObjectsOfType<Light>();
                System.Array.Sort(pointLights, pointLightsDistanceComparer);
            }
        }


        int pointLightsDistanceComparer(Light l1, Light l2) {
            float dist1 = (l1.transform.position - trackingCenter.position).sqrMagnitude;
            float dist2 = (l2.transform.position - trackingCenter.position).sqrMagnitude;
            if (dist1 < dist2) return -1;
            if (dist1 > dist2) return 1;
            return 0;
        }



    }

}                        PointLightManager      VolumetricFogAndMist2   